// SETUP
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
const app = express()
const port = process.env.PORT || 4000
// Mongoose Connection
mongoose.set("strictQuery",false)
mongoose.connect("mongodb+srv://xavieryu:admin123@224-tarriman.lpilq7u.mongodb.net/ecommerce-API?retryWrites=true&w=majority",
    {
        useNewUrlParser:true,
        useUnifiedTopology:true
    
    }

)
let db = mongoose.connection;
db.on("error", ()=> console.error("Connection error"))
db.once("open",()=> console.log("Connected to MongoDB!"))

// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
 
// Main URI
app.use("/users",userRoutes)
app.use("/products",productRoutes)
app.use("/orders", orderRoutes)
app.listen(port, ()=> {console.log(`API is now running at port ${port}`)})