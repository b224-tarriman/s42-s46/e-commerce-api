const express= require("express")
const User = require('../models/User')
const router = express.Router()
const userController = require("../controllers/userController")
const auth = require("../auth")

router.post("/register",(req,res)=>{
    userController.registerUser(req.body)
    .then(result=>res.send(result))
})
router.post("/login", (req,res)=>{
    userController.loginUser(req.body)
    .then(result => res.send(result))
})
router.get("/userDetails",(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    // if(userData.isAdmin){
        userController.getDetails(userData)
        .then(result => res.send(result))
    // }else{
        // res.send({auth:"failed"})
    // }
})

router.put("/setAdmin",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    User.find({isAdmin:true})
    .then(result => {
        console.log(result.length)
        if(result.length === 0){
            userController.firstAdmin(userData)
            .then(result => res.send(result))
        }else if(result.length > 0 && userData.isAdmin){
            userController.setAdmin(req.body)
            .then(result => res.send(result))
        }else{
            res.send({auth:"failed"})
        }
    })
})

module.exports = router