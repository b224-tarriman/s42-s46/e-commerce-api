const express= require("express")
const router = express.Router()
const productController = require("../controllers/productController")
const auth = require("../auth")
const Product = require("../models/Product")


router.post("/addProduct", auth.verify, (req,res)=>{ 
    
    const userData = auth.decode(req.headers.authorization);
    console.log(userData)
    if(userData.isAdmin){
    productController.addProduct(req.body,userData)
    .then(result => res.send(result))
    }else{
        res.send({auth:"failed"})
    }
})
router.get("/allProduct",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        productController.getAllProd(userData)
        .then(result => res.send (result))
    }else{
        productController.getAllActive()
        .then(result => res.send(result))
    }
})

router.get("/",(req,res)=>{
    productController.getAllActive()
    .then(result => res.send(result))
})

router.get("/view/:productId",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    
    productController.getProd(req.params, userData)
    .then(result => res.send(result))
})

router.get("/nonuser/:productId",(req,res)=>{
    productController.singleProd(req.params)
    .then(result => res.send(result))
})

router.put("/update/:productId",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){ 
        productController.updateProd(req.params,req.body)
        .then(result => res.send(result))
    }else{
        res.send({auth:"failed"})
    }
})

router.put("/archive/:productId",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
    productController.archiveProd(req.params)
    .then(result => res.send(result))
    }else{
        res.send("Unauthorized Personnel")
    }

})
module.exports = router