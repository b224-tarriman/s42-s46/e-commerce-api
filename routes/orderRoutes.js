const express= require("express")
const User = require('../models/User')
const router = express.Router()
const orderController = require("../controllers/orderController")
const auth = require("../auth")

// Order Routes

router.post("/checkout",auth.verify,(req,res)=>{
    
    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin){
        res.send({auth:"failed"}) //false
    }else{
        orderController.checkout(req.body,{id:userData.id}).then(result=>res.send(result))
        
    }  
})
//Order Routes
router.get("/userOrders",(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    // if(userData.isAdmin){
        orderController.getUserOrders(userData)
        .then(result => res.send(result))
    // }else{
        // res.send({auth:"failed"})
    // }
})
router.get("/allOrders",(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        orderController.getAllOrders(userData)
        .then(result => res.send(result))
    }else{
        res.send({auth:"failed"})
    }
})

router.post("/addToCart",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    orderController.addToCart(userData,req.body).then(result=>res.send(result))
})
router.post("/deleteFromCart",auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    result = orderController.deleteFromCart(userData,req.body)
    console.log(result.length)
    if(result.length === 0){
        res.send("Product removed! No remaining products in Cart")
    }else{
        res.send("Remaining products"+result)
    }
})

router.post("/checkoutFromCart",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    result = orderController.checkoutFromCart(userData)
    res.send(result)
})
module.exports = router