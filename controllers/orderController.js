const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require("bcrypt")



// Order Controllers


module.exports.checkout = (reqBody,userId)=>{
    return Product.findById(reqBody.products.productId)
    .then(result =>{
        
        if(result !== null && result.stock >= reqBody.products.quantity){
            
            let newOrder = new Order({
                userId: userId.id,
                products:{
                    productId: reqBody.products.productId,
                    quantity: reqBody.products.quantity
                },
                totalAmount: result.price * reqBody.products.quantity
            })
            

            return newOrder.save()
            .then((order,err)=>{
                if(err){
                    return false
                }else{
                    return true
                }
            })
        }else if (!(result.stock <= 0) && result.stock < reqBody.products.quantity){
            return `Not enough stock ${result.stock} remaining`//false
        }else if (result.stock <= 0 ){
            return "Sold out!"
        }else{
            return "Product doesn't exist"
        }
    })
    .then(outcome =>{
        
        if(outcome.stock >= reqBody.products.quantity){
            let updatedStock = outcome.stock - reqBody.products.quantity
            console.log(updatedStock)
        
                return Product.findById(reqBody.products.productId).then((result,err)=>{
                    if(err){
                        return false
                    }else{
                    result.stock = updatedStock
                    return result.save().then((update,err)=>{
                        if(err){
                            return false
                        }else{
                            return true
                        }
                    })
                    }
                })
        
        }else{
            return true
        }
    })
};


//GET UserOrders
module.exports.getUserOrders = (data) =>{
    return Order.find({userId:data.id}).then(result => {
        console.log(result)
        if (result == null){
            return false
        }else{
            return result
        }
	})
}
// GET AllOrders
module.exports.getAllOrders = (data) =>{
    return Order.find({}).then(result => {
        console.log(result)
        if (result == null){
            return false
        }else{
            return result
        }
	})
}

let addCart = []

// -STRETCH GOAL- 
module.exports.addToCart = (data,reqBody)=>{
   return Product.findById(reqBody.productId).then(result=>{
      
        if(addCart.length === 0){
            let newCart = {
                userId:data.id,
                prodId:result.id,
                price:result.price,
                quantity:reqBody.quantity,
                subTotal:reqBody.quantity * result.price,
                total: reqBody.quantity * result.price
            }

            addCart.push(newCart)
        }else{
            index = addCart.findIndex((obj => obj.prodId == reqBody.productId))
            console.log(index)
            let x = addCart.length -1
            if(data.id === addCart[0].userId){
                    if(index >= 0){
                        addCart[index]={
                            userId:data.id,
                            prodId:result.id,
                            price:result.price,
                            quantity:reqBody.quantity,
                            subTotal:reqBody.quantity * result.price,
                            total: (reqBody.quantity * result.price)
                        }
                        return addCart
                    }else{

                        let newCart = {
                            userId:data.id,
                            prodId:result.id,
                            price:result.price,
                            quantity:reqBody.quantity,
                            subTotal:reqBody.quantity * result.price,
                            total: addCart[x].total + (reqBody.quantity * result.price)
                        }
                      
                        addCart.push(newCart)
                    }
            }else{
                addCart = []
                let newCart = {
                    userId:data.id,
                    prodId:result.id,
                    price:result.price,
                    quantity:reqBody.quantity,
                    subTotal:reqBody.quantity * result.price,
                    total: reqBody.quantity * result.price
                }
    
                addCart.push(newCart)
            }
        }
           
            return addCart
    })
}

module.exports.deleteFromCart = (data,reqBody)=>{
    index = addCart.findIndex((obj => obj.prodId == reqBody.productId))
    removedProduct = addCart.splice(index,1);
    return addCart
}


module.exports.checkoutFromCart = (data)=>{
    console.log(addCart.length)
    if(addCart.length > 0 && data.id === addCart[0].userId){
        for(let i =0;i < addCart.length;i++){
            console.log(addCart[i].userId)

            let newOrder = new Order({
                userId: addCart[i].userId,
                products:{
                    productId: addCart[i].prodId,
                    quantity: addCart[i].quantity
                },
                totalAmount: addCart[i].subTotal
            })
            console.log(newOrder)
            newOrder.save().then((order,err)=>{
                if(err){
                    return false
                }else{
                    return result//true
                }
            })
            Product.findById(addCart[i].prodId).then((result,err)=>{
                if(err){
                    return false
                }else{
                result.stock -= addCart[i].quantity
                result.save().then((update,err)=>{
                    if(err){
                        return false
                    }else{
                        return true
                    }
                })
                }
            })
        }
    }else{
    return "No existing product in the cart"
    }
}