const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.registerUser=(reqBody)=>{
    return User.find({email:reqBody.email})
    .then(result => {
        if(result.length > 0){
            return false
        }else{
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                password:bcrypt.hashSync(reqBody.password, 10)
            })
    
            return newUser.save()
            .then((user,err)=>{
                if(err){
                    // user registration failed
                    return false
                }else{
                    // successful registration
                    return true
                }
            })
        }
    })
};

module.exports.loginUser = (reqBody)=>{
    return User.findOne({email:reqBody.email})
    .then(result=> {
        if (result == null){
            return false
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            }else{
                return false
            }
        }
    })
};


module.exports.getDetails = (data) =>{
    return User.findById(data.id).then(result => {
        if (result == null){
            return false
        }else{
            result.password = "*****"
            return result
        }
	})
}



module.exports.setAdmin = async (reqBody) =>{
    let updatedAdmin = {
        isAdmin: reqBody.isAdmin
    }
    try{
    await User.findByIdAndUpdate(reqBody.userId,updatedAdmin)
    return `Admin created`
    }catch(error){
        return false
    }
}
module.exports.firstAdmin = async (data) =>{
    let updatedAdmin = {
        isAdmin: true
    }
    try{
    await User.findByIdAndUpdate(data.id,updatedAdmin)
    return `First admin created`
    }catch(error){
        return false
    }
}
