const Product = require('../models/Product')
const User = require('../models/User')
const auth = require("../auth")
const bcrypt = require("bcrypt")



module.exports.addProduct = (reqBody,data) =>{
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stock: reqBody.stock
    })
    return newProduct.save().then((product,err)=>{
        if(err){
            return false
        }else{
            return true
        }
    })
}
module.exports.getAllProd = (data) =>{
        return Product.find({}).then(result =>{
            return result
        })
}
module.exports.getAllActive = () =>{
    return Product.find({isActive:true},{name:1,description:1,price:1,stock:1}).then(result=>{
        return result
    })
}

module.exports.getProd = (id,data) =>{
    if(data.isAdmin){
    return Product.findById(id.productId)
    .then(result=> {return result})
    }else{
    return Product.findById(id.productId)
    .then(result=> {
        if(result.isActive){
            return result
        }else{

            return false
        }
    })
    }
}
module.exports.singleProd = (id) =>{
    return Product.findById(id.productId)
    .then(result=> {
        if(result.isActive){
            return result
        }else{

            return false
        }
    })
}
module.exports.updateProd = async (reqParams,reqBody)=>{
    const filter = Product.findById(reqParams.productId)
    const update = reqBody
    return await Product.findOneAndUpdate(filter,update).then((success,error)=>{
        if(error){
            return false
        }else{
            return true
        }
    })
}

module.exports.archiveProd = (reqParams)=>{
    try{
     return Product.findById(reqParams.productId)
        .then(result =>{
            if(result.isActive){
                result.isActive = false
                return result.save().then((archived)=>{
                    if(archived){
                        return true
                    }else{
                        return false
                    }
                })
            }else{
                result.isActive = true
                return result.save().then((activated)=>{
                    if(activated){
                        return true
                    }else{
                        return false
                    }
                })
            }
        })
    }catch(error){
        return false
    }
}